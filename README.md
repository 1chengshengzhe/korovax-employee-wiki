# README

Extracted wiki template from GitLab pages examples. 

This POC will be complementary to the whole suite of offerings which KoroVax will use internally.

Todo:
    - The employee wiki will list what each employee is responsible for, eg, Josh will be in charge of the krs-admin-portal
    - Please note that not all repository should be made public, relevant ones with business data should be made private

POC Site: https://korovax.gitlab.io/korovax-employee-wiki